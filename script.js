"use strict";

function createBtns () {
    const btnStop = document.createElement("button");
    btnStop.textContent = "Припинити";
    btnStop.classList.add("btn");
    btnStop.setAttribute("id","stop");
    document.body.append(btnStop);

    const btnPlay = document.createElement("button");
    btnPlay.textContent = "Відновити показ";
    btnPlay.classList.add("btn");
    btnPlay.setAttribute("id","play");
    document.body.append(btnPlay);
}

createBtns ();

const images = document.querySelectorAll(".image-to-show");
const btnStop = document.getElementById("stop");
const btnPlay = document.getElementById("play");
let i = 0;
let timer = null;

function changeImg (images) {
    images[i].classList.remove("active");
    if (i >= images.length-1) {
        i = 0;
    } else {
        i++;
    }
    images[i].classList.add("active");
}

function startChangeImg(changeImg, images) {
    timer = setInterval(changeImg, 3000, images);
    btnPlay.disabled = true;
    btnStop.disabled = false;
}

function stopChangeImg(timer) {
    clearInterval(timer);
    btnPlay.disabled = !btnPlay.disabled;
    btnStop.disabled = !btnStop.disabled;
}

startChangeImg(changeImg, images);

btnStop.addEventListener('click', () => {
    stopChangeImg(timer);
})

btnPlay.addEventListener('click', () => {
    startChangeImg(changeImg, images);
})